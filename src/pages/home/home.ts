import {Platform} from 'ionic-angular';
import {Component, NgZone} from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';
import moment from 'moment';

import {ID_KEY, ID_SECRET} from '../../constants/indooratlas';
import {WAYPOINTS} from "../../constants/locations";

declare let IndoorAtlas: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  watchID: string;
  map: GoogleMap;
  marker: Marker;
  position: any;
  nearestWaypoint: string;
  lastWaypoint: string;
  lastWaypointMoment: moment.Moment;

  constructor(private googleMaps: GoogleMaps, public platform: Platform, public zone: NgZone) {
    platform.ready().then(
      () => {
        this.loadMap();
      }
    )
  }

  setPosition(position) {
    this.position = position;
    this.nearestWaypoint = this.getNearestWaypoint(position);
    if (this.lastWaypoint != this.nearestWaypoint) {
      this.onWaypointUpdate(this.nearestWaypoint);
    }
  }

  getNearestWaypoint(position) {
    let lat = position.coords.latitude;
    let lng = position.coords.longitude;
    if (WAYPOINTS.length == 0) {
      return "No Waypoint Available";
    }
    else if (WAYPOINTS.length == 1) {
      return WAYPOINTS[0].name;
    }
    let dist = WAYPOINTS.map((p) => Math.sqrt((p.lat - lat) * (p.lat - lat) + (p.lng - lng) * (p.lng - lng)));
    let minDist = Math.min(...dist);
    return WAYPOINTS[dist.indexOf(minDist)].name;
  }

  loadMap() {
    // console.log('loading map');
    // let location = new LatLng(1.3334158, 103.7358111);
    let location = new LatLng(1.291539, 103.769920);
    let position: CameraPosition = {
      target: location,
      zoom: 20,
      tilt: 30
    };

    let element: HTMLElement = document.getElementById('map');

    this.map = this.googleMaps.create(element);

    this.map.one(GoogleMapsEvent.MAP_READY).then(
      () => {
        console.log('Map is ready!');
        this.map.moveCamera(position);
        let markerOptions: MarkerOptions = {
          position: location
        };

        this.map.addMarker(markerOptions).then((marker: Marker) => {
          this.marker = marker;
        });

        this.initIndoorAtlas();
      }
    );
  }

  initIndoorAtlas() {
    IndoorAtlas.initialize(
      () => {
        // console.log("IndoorAtlas init success");
        setTimeout(() => {
          this.watchID = IndoorAtlas.watchPosition(
            (position) => {
              console.log('Latitude: ' + position.coords.latitude + ', Longitude: ' + position.coords.longitude);
              this.zone.run(() => {
                this.setPosition(position);
              });
              this.onPositionUpdate(new LatLng(position.coords.latitude, position.coords.longitude), this.marker);
            },
            (error) => {
              console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
            },
            {});
        }, 1000);
      },
      (error) => {
        console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
      },
      {
        key: ID_KEY,
        secret: ID_SECRET
      })
  }

  onPositionUpdate(position, marker: Marker) {
    marker.setPosition(position);
  }

  onWaypointUpdate(waypoint: string) {
    this.lastWaypoint = waypoint;
    this.lastWaypointMoment = moment();
  }

}
