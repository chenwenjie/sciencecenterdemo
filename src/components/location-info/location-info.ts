import {Component, Input} from '@angular/core';

import moment from 'moment';


@Component({
  selector: 'location-info',
  templateUrl: 'location-info.html'
})
export class LocationInfoComponent {
  @Input() nearestWaypoint: string;
  @Input() lastWaypointMoment = moment();
  @Input() position: any;
  duration: string;

  constructor() {
    setInterval(() => {
      if (!this.lastWaypointMoment) {
        this.duration = "NA"
      } else {
        this.duration = Math.floor(moment.duration(moment().diff(this.lastWaypointMoment), 'milliseconds').asSeconds()) + " seconds";
      }
    }, 1000)
  }

}
